<?php
    namespace Some\Package;
    use Zimplify\Core\Instance;

    /**
     * <add the instance description here>
     * @package <package name> (code <prefix>)
     * @type instance (code 01)
     * @file SomeClass (code <prefix>)
     */
    class SomeClass extends Instance {

        /**
         * this is the override for the initialization
         * @return void
         */
        protected function prepare() {

            //do not remove line! need for event controls
            parent::prepare();

        }

        /**
         * this is to override the standard presave routine for first-time saving. if need to do something everytime during a save
         * make sure you add a handler for before-save event and trigger from there. 
         * @return void
         */
        protected function presave() {

            // do not remove this line! need to make sure all basic data sync
            parent::presave();

            // add your other first time saving data checks or presets.
        }
    }