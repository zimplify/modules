# Welcome to Zimplify Module

It's time to make a new module! There are several things you need to do in order to get things going.

1. Remove the origin git destination

```
git remote remove origin
```

1. Add your intended git repo location

```
git remote add origin <your git location>
```

1. Run composer

```
composer init
```

1. Add the classes, providers, activities, etc to your project
1. Update package.json
1. Make sure you commit to your repo

```
git push origin <branch>
```

1. Finally make sure you add the repo into to composer.json of your main project in order to access the repo
